import { StyleSheet } from 'react-native';
import * as defaultStyle from '../style';

export default function getStyle(theme = {}) {
  const appStyle = { ...defaultStyle, ...theme };
  return StyleSheet.create({
    container: {
      paddingLeft: 0,
      paddingRight: 0,
      flex: 1,
      backgroundColor: appStyle.calendarBackground
    },
    week: {
      marginTop: 2,
      marginBottom: 2,
      flexDirection: 'row',
      justifyContent: 'space-around'
    },
    highlightedWeek: {
      backgroundColor: appStyle.highlightWeekBackgroundColor
    }
  });
}
